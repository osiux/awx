SHELL:=/bin/bash

DEBUG                  ?= -vv
SUDO                   ?=
GIT_TYPE               ?= $$(git config --get remote.origin.url | grep -o http)
GIT_HTTP               ?= $$(git config --get remote.origin.url | grep http | rev | cut -d/ -f2- | rev)
GIT_HOST               ?= $$(git config --get remote.origin.url | cut -d: -f1)
GIT_BASE               ?= $$(git config --get remote.origin.url | cut -d: -f2 | cut -d/ -f2)
GIT_URL                ?= $$([[ "$(GIT_TYPE)" = 'http' ]] && echo $(GIT_HTTP) || echo $(GIT_HOST):/$(GIT_BASE))
AWX_ENV                ?= develop
AWX_GITLAB_USER        ?= awx_gitlab_test
AWX_GITLAB_JOB_LAUNCH  ?= undefined_job_template
AWX_GITLAB_JOB_LIMIT   ?= undefined_limit
AWX_REPO               ?= awx
AWX_DIR                ?= .
AWX_REVISION           ?= develop
AWX_DEPLOY_TYPE        ?= update
TOOLS_REPO             ?= ansible_tools
TOOLS_REVISION         ?= develop
TOOLS_DIR              ?= ansible_tools

lint_all: lint_inventory lint_project lint_job_template lint_workflow

lint_inventory:
	find inventory -type f -iname '*.json' | xargs jsonlint -v

lint_project:
	find project   -type f -iname '*.json' | xargs jsonlint -v

lint_job_template:
	find job_template -type f -iname '*.json' | xargs jsonlint -v

lint_workflow:
	find workflow     -type f -iname '*.json' | xargs jsonlint -v

deploy: tools_env awx_deploy

awx_deploy:
	echo "export PATH="$$PWD/$(TOOLS_DIR):$$PATH"" > .tools
	source .tools && awx-config $(AWX_ENV)
	source .tools && awx-deploy-revision $(AWX_DIR) $(AWX_REVISION) $(AWX_DEPLOY_TYPE)

tools:
	[[ -d $(TOOLS_DIR) ]] || git clone $(GIT_URL)/$(TOOLS_REPO) $(TOOLS_DIR)

tools_env: tools
	cd $(TOOLS_DIR) && git checkout $(TOOLS_REVISION);git pull

pre-commit:
	pre-commit run

awx_config:
	echo "export PATH="$$PWD/$(TOOLS_DIR):$$PATH"" > .tools
	source .tools && awx-config $(AWX_ENV)

awx_repo:
	[[ -d $(AWX_REPO) ]] || git clone $(GIT_URL)/$(AWX_REPO) $(AWX_DIR)

awx_version: awx_config
	awx-cli version

awx_user:
	awx-cli user get --username $(AWX_GITLAB_USER)
