# `awx.git`

_AWX_ resource repository in `.json` files.

## License

GNU General Public License, GPLv3.

## Author Information

This repo was created in 2022 by
 [Osiris Alejandro Gomez](https://osiux.com/), worker cooperative of
 [gcoop Cooperativa de Software Libre](https://www.gcoop.coop/).
